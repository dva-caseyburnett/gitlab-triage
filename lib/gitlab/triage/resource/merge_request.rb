# frozen_string_literal: true

require_relative 'base'
require_relative 'shared/issuable'

module Gitlab
  module Triage
    module Resource
      class MergeRequest < Base
        include Shared::Issuable

        def reference
          '!'
        end

        def first_contribution?
          if resource.key?(:first_contribution)
            resource[:first_contribution]
          else
            expanded = expand_resource!
            expanded[:first_contribution]
          end
        end
      end
    end
  end
end
