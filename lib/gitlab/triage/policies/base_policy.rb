# frozen_string_literal: true

module Gitlab
  module Triage
    module Policies
      class BasePolicy
        attr_reader :type, :policy_spec, :resources, :network

        def initialize(type, policy_spec, resources, network)
          @type = type
          @policy_spec = policy_spec
          @resources = resources
          @network = network
        end

        def name
          @name ||= (policy_spec[:name] || "#{type}-#{object_id}")
        end

        def actions
          @actions ||= policy_spec.fetch(:actions) { {} }
        end

        def summarize?
          actions.key?(:summarize)
        end

        def comment?
          # The actual keys are strings
          (actions.keys.map(&:to_sym) - [:summarize]).any?
        end

        def build_issue
          raise NotImplementedError
        end
      end
    end
  end
end
